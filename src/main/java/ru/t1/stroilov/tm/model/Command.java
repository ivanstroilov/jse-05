package ru.t1.stroilov.tm.model;

public class Command {

    private String argument;

    private String description;

    private String name;

    public Command() {

    }

    public Command(final String name, final String argument, final String description) {
        setName(name);
        setArgument(argument);
        setDescription(description);
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;

        return result;
    }

}